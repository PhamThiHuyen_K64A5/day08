<!DOCTYPE html>
<html lang='en'>

<head>
    <meta charset='UTF-8'>
    <meta http-equiv='X-UA-Compatible' content='IE=edge'>
    <meta name='viewport' content='width=device-width, initial-scale=1.0'>
    <title>Confirm</title>
    <link rel='stylesheet' type='text/css' href='confirm.css'>
</head>

<body>
    <form>
        <?php
        session_start();
        ?>
        <table>
            <tr>
                <td>
                    <div class="green_background"><label> Họ và tên </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["name"]; ?></lable>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="green_background"><label> Giới tính </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["gender"]; ?></lable>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="green_background"><label> Phân khoa </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["faculty"]; ?></lable>
                </td>
            </tr>
            <tr>
                <td>
                    <div class="green_background"><label> Ngày sinh </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["dateBirth"]; ?></lable>
                </td>
            </tr>

            <tr>
                <td>
                    <div class="green_background"><label> Địa chỉ </label></div>
                </td>
                <td>
                    <lable><?php echo $_SESSION["address"]; ?></lable>
                </td>
            </tr>
            <tr>
                <td valign="top">
                    <div class="green_background"><label> Hình ảnh </label></div>
                </td>
                <td>
                    <img src='<?php echo $_SESSION["image"] ?>' width='90px' height="60px" />
                </td>
            </tr>
        </table>
        <input type='submit' name='submit' value='Xác nhận' class='submit'>
    </form>

</body>

</html>